import sys, struct

def DataToBitsList(Data):
    BitsList = []
    for Elem in xrange(len(Data)):
        Char = Data[Elem]
        for x in xrange(7, -1, -1):
            BitsList.append((ord(Char) >> x) & 1)
    return BitsList

def BitsListToData(BitsList, Format, Len):
    OutData = ''
    for Elem in xrange(0, len(BitsList), Len):
        Byte = BitsList[Elem:Elem + Len]
        Byte = ''.join(str(Bit) for Bit in Byte)
        PadLen = Len - len(Byte)
        OutData += struct.pack(Format, int(Byte, 2) << PadLen)
    return OutData, PadLen

def CreateHuffmanTree(Data):
    BytesFreqDict = {}
    for Elem in Data:
        if (Elem not in BytesFreqDict):
            BytesFreqDict[Elem] = 1
        else:
            BytesFreqDict[Elem] += 1

    NodeList = sorted(BytesFreqDict.items(), key = lambda field: field[1], reverse = True)

    while (len(NodeList) > 1):
        Left = NodeList.pop()
        Right = NodeList.pop()
        Parent = []
        Parent.append(Right)
        Parent.append(Left[1] + Right[1])
        Parent.append(Left)
        NodeList.append(Parent)
        NodeList = sorted(NodeList, key = lambda field: field[1], reverse = True)
    return NodeList

def CreateHuffmanDict(List, Symbol = '', SymDict = {}):
    if (len(List[0]) == 3):
        CreateHuffmanDict(List[0], Symbol + '0')
    elif (len(List[0]) == 2):
        SymDict[List[0][0]] = Symbol + '0'
    if (len(List[2]) == 3):
        CreateHuffmanDict(List[2], Symbol + '1')
    elif (len(List[2]) == 2):
        SymDict[List[2][0]] = Symbol + '1'
    return SymDict

def EncodeTree(List):
    SymList = []
    if (len(List[0]) == 3):
        SymList.append(0)
        for Elem in (EncodeTree(List[0])):
            SymList.append(Elem)
    elif (len(List[0]) == 2):
        SymList.append(1)
        for Elem in DataToBitsList(List[0][0]):
            SymList.append(Elem)
    if (len(List[2]) == 3):
        SymList.append(0)
        for Elem in (EncodeTree(List[2])):
            SymList.append(Elem)
    elif (len(List[2]) == 2):
        SymList.append(1)
        for Elem in DataToBitsList(List[2][0]):
            SymList.append(Elem)
    return SymList

def HuffmanEncode(Data):
    HuffmanTree = CreateHuffmanTree(Data)
    CodesDict = CreateHuffmanDict(HuffmanTree[0])
    EncodedTreeList = EncodeTree(HuffmanTree[0])
    EncodedTree = BitsListToData(EncodedTreeList, '>B', 8)[0]

    Message = ''
    for Elem in Data:
        Message += CodesDict[Elem]

    OutData, PadLen = BitsListToData(Message, '>I', 32)
    OutData = struct.pack('>H', len(EncodedTreeList)) + EncodedTree + chr(PadLen) + OutData
    return OutData

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

OutData = HuffmanEncode(InData)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(OutData)
OutFile.close()
