import sys, Queue

InFile = open(sys.argv[1])
InData = InFile.read()
InFile.close()

# InData = 'this is an example of a huffman tree'

class HuffmanNode(object):
    def __init__(self, left = None, right = None):
        self.left = left
        self.right = right
    def children(self):
        return ((self.left, self.right))

Dict = {}
for Elem in InData:
    if (Elem not in Dict):
        Dict[Elem] = 1
    else:
        Dict[Elem] += 1

SortedList = sorted(Dict.items(), key = lambda field: field[1], reverse = True)
for Elem in xrange(len(SortedList)):
    SortedList[Elem] = (SortedList[Elem][1], SortedList[Elem][0])

PQueue = Queue.PriorityQueue()
for Elem in SortedList:
    PQueue.put(Elem)
while (PQueue.qsize() > 1):
    Left = PQueue.get()
    Right = PQueue.get()
    Node = HuffmanNode(Left, Right)
    PQueue.put((Left[0] + Right[0], Node))
node = PQueue.get()

def walk_tree(node, prefix = '', code = {}):
    if isinstance(node[1].left[1], HuffmanNode):
        walk_tree(node[1].left, prefix + '0', code)
    else:
        code[node[1].left[1]] = prefix + '0'
    if isinstance(node[1].right[1], HuffmanNode):
        walk_tree(node[1].right, prefix + '1', code)
    else:
        code[node[1].right[1]] = prefix + '1'
    return code

code = walk_tree(node)
for i in sorted(SortedList, reverse = True):
    print i[1], i[0], code[i[1]]
