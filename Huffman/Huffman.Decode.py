import sys, struct, math

def DataToBitsList(Data):
    BitsList = []
    for Elem in xrange(len(Data)):
        Char = Data[Elem]
        for x in xrange(7, -1, -1):
            BitsList.append((ord(Char) >> x) & 1)
    return BitsList

def BitsListToData(BitsList, Format, Len):
    OutData = ''
    for Elem in xrange(0, len(BitsList), Len):
        Byte = BitsList[Elem:Elem + Len]
        Byte = ''.join(str(Bit) for Bit in Byte)
        PadLen = Len - len(Byte)
        OutData += struct.pack(Format, int(Byte, 2) << PadLen)
    return OutData, PadLen

def RebuildHuffmanTree(EncodedTree, Index = 0):
    Parent = []
    NewIndex = 0
    if (EncodedTree[Index] == 0):
        NewIndex, Left = RebuildHuffmanTree(EncodedTree, Index + 1)
    else:
        Left = BitsListToData(EncodedTree[Index + 1:Index + 9], '>B', 8)[0]
        Index += 9
    if (NewIndex > Index):
        Index = NewIndex
    if (EncodedTree[Index] == 0):
        NewIndex, Right = RebuildHuffmanTree(EncodedTree, Index + 1)
    else:
        Right = BitsListToData(EncodedTree[Index + 1:Index + 9], '>B', 8)[0]
        Index += 9
    Parent.append(Left)
    Parent.append(Right)
    if (NewIndex > Index):
        return NewIndex, Parent
    else:
        return Index, Parent

def FindSymbol(HuffmanTree, BitsList):
    Direction = BitsList.pop()
    while (isinstance(HuffmanTree[Direction], str) == False):
        HuffmanTree = HuffmanTree[Direction]
        Direction = BitsList.pop()
    return HuffmanTree[Direction], BitsList

def HuffmanDecode(Data):
    DataIndex = 0
    TreeLen = struct.unpack('>H', Data[0] + Data[1])[0]
    DataIndex += 2
    TreeLenBytes = int(math.ceil(TreeLen / float(8)))
    EncodedTree = Data[DataIndex:TreeLenBytes + DataIndex]
    DataIndex += TreeLenBytes
    PadLen = struct.unpack('>B', Data[DataIndex])[0]
    DataIndex += 1
    Data = Data[DataIndex:]

    EncodedTree = DataToBitsList(EncodedTree)[:TreeLen]
    HuffmanTree = RebuildHuffmanTree(EncodedTree)[1]
    BitsList = DataToBitsList(Data)
    BitsList = BitsList[::-1]
    CharBuf = ''
    while (len(BitsList) > PadLen):
        Char, BitsList = FindSymbol(HuffmanTree, BitsList)
        CharBuf += Char
    return CharBuf

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

DecodedData = HuffmanDecode(InData)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(DecodedData)
OutFile.close()
