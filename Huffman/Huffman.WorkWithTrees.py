import sys, struct

def CreateHuffmanTree(Data):
    BytesFreqDict = {}
    for Elem in Data:
        if (Elem not in BytesFreqDict):
            BytesFreqDict[Elem] = 1
        else:
            BytesFreqDict[Elem] += 1

    NodeList = sorted(BytesFreqDict.items(), key = lambda field: field[1], reverse = True)

    while (len(NodeList) > 1):
        Left = NodeList.pop()
        Right = NodeList.pop()
        Parent = []
        Parent.append(Right)
        Parent.append(Left[1] + Right[1])
        Parent.append(Left)
        NodeList.append(Parent)
        NodeList = sorted(NodeList, key = lambda field: field[1], reverse = True)
    return NodeList

def CreateHuffmanDict(List, Symbol = '', SymDict = {}):
    if (len(List[0]) == 3):
        CreateHuffmanDict(List[0], Symbol + '0')
    elif (len(List[0]) == 2):
        SymDict[List[0][0]] = Symbol + '0'
    if (len(List[2]) == 3):
        CreateHuffmanDict(List[2], Symbol + '1')
    elif (len(List[2]) == 2):
        SymDict[List[2][0]] = Symbol + '1'
    return SymDict

def EncodeTree(List, SymList = []):
    if (len(List[0]) == 3):
        SymList.append('0')
        EncodeTree(List[0])
    elif (len(List[0]) == 2):
        SymList.append('1' + List[0][0])
    if (len(List[2]) == 3):
        SymList.append('0')
        EncodeTree(List[2])
    elif (len(List[2]) == 2):
        SymList.append('1' + List[2][0])
    return SymList

def RebuildHuffmanTree(EncodedTree, Index = 0):
    Parent = []
    NewIndex = 0
    if (EncodedTree[Index] == '0'):
        NewIndex, Left = RebuildHuffmanTree(EncodedTree, Index + 1)
    else:
        Left = EncodedTree[Index + 1]
        Index += 2
    if (NewIndex > Index):
        Index = NewIndex
    if (EncodedTree[Index] == '0'):
        NewIndex, Right = RebuildHuffmanTree(EncodedTree, Index + 1)
    else:
        Right = EncodedTree[Index + 1]
        Index += 2
    Parent.append(Left)
    Parent.append(Right)
    if (NewIndex > Index):
        return NewIndex, Parent
    else:
        return Index, Parent

def RebuildHuffmanDict(List, Symbol = '', SymDict = {}):
    if (len(List[0]) == 2):
        RebuildHuffmanDict(List[0], Symbol + '0')
    elif (len(List[0]) == 1):
        SymDict[List[0][0]] = Symbol + '0'
    if (len(List[1]) == 2):
        RebuildHuffmanDict(List[1], Symbol + '1')
    elif (len(List[1]) == 1):
        SymDict[List[1][0]] = Symbol + '1'
    return SymDict

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

# InData = 'this is an example of a huffman tree'

HuffmanTree = CreateHuffmanTree(InData)
CodesDict = CreateHuffmanDict(HuffmanTree[0])
EncodedTree = ''.join(EncodeTree(HuffmanTree[0]))

NotUseful, RebuiltHuffmanTree = RebuildHuffmanTree(EncodedTree)
RebuiltCodesDict = RebuildHuffmanDict(RebuiltHuffmanTree)
assert (CodesDict == RebuiltCodesDict)
# print RebuiltCodesDict

Message = ''
for Elem in InData:
    Message += CodesDict[Elem]

print Message
    
OutFile = open(sys.argv[2], 'wb')
OutFile.write(struct.pack('>H', len(EncodedTree)))
OutFile.write(EncodedTree)
OutFile.write('0')

for Elem in xrange(0, len(Message), 8):
    Byte = Message[Elem:Elem + 8]
    PadLen = (8 % len(Byte))
    OutFile.write(struct.pack('>B', int(Byte, 2) << PadLen))

OutFile.seek(2 + len(EncodedTree))
OutFile.write(struct.pack('>B', PadLen))
OutFile.close()
