import sys, struct, math

# Since we are using runlength fields of up to 2 bytes long
# 65535 is the max value we can store in them, so divide
# the input stream in blocks of that length.
BlockSize = 65535

def BWTDecode(InData):
    k = struct.unpack('>H', InData[:2])[0]
    s = InData[2:]
    def Row(k):
        Permutation = sorted((t, i) for i, t in enumerate(s))
        for _ in s:
            t, k = Permutation[k]
            yield t
    return ''.join(Row(k))

def MTFDecode(Data):
    Dict = []
    NewList = []
    for Elem in xrange(256):
        Dict.append(chr(Elem))
    for Elem in Data:
        Index = ord(Elem)
        NewList.append(Dict[Index])
        Dict.insert(0, Dict[Index])
        Dict.pop(Index + 1)
    return ''.join(NewList)

def DataToBitsList(Data):
    BitsList = []
    for Elem in xrange(len(Data)):
        Char = Data[Elem]
        for x in xrange(7, -1, -1):
            BitsList.append((ord(Char) >> x) & 1)
    return BitsList

def BitsListToData(BitsList, Format, Len):
    OutData = ''
    for Elem in xrange(0, len(BitsList), Len):
        Byte = BitsList[Elem:Elem + Len]
        Byte = ''.join(str(Bit) for Bit in Byte)
        PadLen = Len - len(Byte)
        OutData += struct.pack(Format, int(Byte, 2) << PadLen)
    return OutData, PadLen

def RebuildHuffmanTree(EncodedTree, Index = 0):
    Parent = []
    NewIndex = 0
    if (EncodedTree[Index] == 0):
        NewIndex, Left = RebuildHuffmanTree(EncodedTree, Index + 1)
    else:
        Left = BitsListToData(EncodedTree[Index + 1:Index + 9], '>B', 8)[0]
        Index += 9
    if (NewIndex > Index):
        Index = NewIndex
    if (EncodedTree[Index] == 0):
        NewIndex, Right = RebuildHuffmanTree(EncodedTree, Index + 1)
    else:
        Right = BitsListToData(EncodedTree[Index + 1:Index + 9], '>B', 8)[0]
        Index += 9
    Parent.append(Left)
    Parent.append(Right)
    if (NewIndex > Index):
        return NewIndex, Parent
    else:
        return Index, Parent

def FindSymbol(HuffmanTree, BitsList):
    Direction = BitsList.pop()
    while (isinstance(HuffmanTree[Direction], str) == False):
        HuffmanTree = HuffmanTree[Direction]
        Direction = BitsList.pop()
    return HuffmanTree[Direction], BitsList

def HuffmanDecode(Data):
    DataIndex = 0
    TreeLen = struct.unpack('>H', Data[0] + Data[1])[0]
    DataIndex += 2
    TreeLenBytes = int(math.ceil(TreeLen / float(8)))
    EncodedTree = Data[DataIndex:TreeLenBytes + DataIndex]
    DataIndex += TreeLenBytes
    PadLen = struct.unpack('>B', Data[DataIndex])[0]
    DataIndex += 1
    Data = Data[DataIndex:]

    EncodedTree = DataToBitsList(EncodedTree)[:TreeLen]
    HuffmanTree = RebuildHuffmanTree(EncodedTree)[1]
    BitsList = DataToBitsList(Data)
    BitsList = BitsList[::-1]
    CharBuf = ''
    while (len(BitsList) > PadLen):
        Char, BitsList = FindSymbol(HuffmanTree, BitsList)
        CharBuf += Char
    return CharBuf

InFile = open(sys.argv[1], 'rb')
OutFile = open(sys.argv[2], 'wb')

BlocksNum = struct.unpack('>H', InFile.read(2))[0]
BlocksHeader = InFile.read(BlocksNum * 4)

BlocksList = []
FormatString = '>%dI' % BlocksNum
BlocksInfos = struct.unpack(FormatString, BlocksHeader)
for Elem in BlocksInfos:
    TransBits = Elem >> 24
    BlockSize = Elem & 0xFFFFFF
    BlocksList.append((TransBits, BlockSize))

for Elem in BlocksList:
    Data = InFile.read(Elem[1])
    DecodedData = Data
    if ((Elem[0] >> 0) & 1 == 1):
        DecodedData = HuffmanDecode(DecodedData)
    if ((Elem[0] >> 1) & 1 == 1):
        DecodedData = MTFDecode(DecodedData)
    if ((Elem[0] >> 2) & 1 == 1):
        DecodedData = BWTDecode(DecodedData)
    OutFile.write(DecodedData)

InFile.close()
OutFile.close()
