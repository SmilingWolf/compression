import sys, struct, math

# Since we are using runlength fields up to 2 bytes long
# 65535 is the max value we can store in them, so divide
# the input stream in blocks of that length.
BlockSize = 65535

def MTFEncode(Data):
    Dict = []
    NewList = []
    for Elem in xrange(256):
        Dict.append(chr(Elem))
    for Elem in Data:
        Index = Dict.index(Elem)
        NewList.append(chr(Index))
        Dict.pop(Index)
        Dict.insert(0, Elem)
    return ''.join(NewList)

def GetRunLength(Data, Index):
    RunLength = 1
    while (Data[Index] == Data[Index + 1]):
        RunLength += 1
        Index += 1
        if (Index + 1 == len(Data)):
            break
    return RunLength

def RLEEncode(Data):
    DataIndex = 0
    RunCounter = 0
    TupleList = []
    OutData = b''
    while (DataIndex + 1 < len(Data)):
        RunCounter = GetRunLength(Data, DataIndex)
        if (RunCounter <= 2):
            Digram = Data[DataIndex] + Data[DataIndex + 1]
            EncodedTuple = (2, Digram)
            if (RunCounter == 1):
                DataIndex += 1
        else:
            EncodedTuple = (RunCounter, Data[DataIndex])
        TupleList.append(EncodedTuple)
        DataIndex += RunCounter
    if (DataIndex < len(Data)):
        EncodedTuple = (1, Data[DataIndex])
        TupleList.append(EncodedTuple)
        DataIndex += 1
    for Elem in TupleList:
        if (Elem[0] == 1):
            OutData += '\x00'
        elif (Elem[0] == 2):
            OutData += '\x01'
        elif (Elem[0] > 2 and Elem[0] < 256):
            OutData += '\x02'
            OutData += struct.pack('>B', Elem[0])
        else:
            OutData += '\x03'
            OutData += struct.pack('>H', Elem[0])
        OutData += Elem[1]
    return OutData

def ApplySelected(Data, TransBits):
    BoolCompress = TransBits & 1
    BoolMTF = (TransBits >> 1) & 1
    Encoded = Data
    if (BoolMTF == 1 and BoolCompress == 1):
        Encoded = MTFEncode(Encoded)
    if (BoolCompress == 1):
        Encoded = RLEEncode(Encoded)
    return Encoded

def CompressBrute(Data):
    CompressResDict = {}
    for TransBits in xrange(4):
        CompressResDict[TransBits] = len(ApplySelected(Data, TransBits))
    CompressResults = sorted(CompressResDict.items(), key = lambda field: field[1])
    return CompressResults[0][0]

InFile = open(sys.argv[1], 'rb')
OutFile = open(sys.argv[2], 'wb')

InFile.seek(0, 2)
FileSize = InFile.tell()
InFile.seek(0, 0)

BlocksHeader = b''
BlocksNum = int(math.ceil(FileSize / float(BlockSize)))
OutFile.write(struct.pack('>H', BlocksNum))
OutFile.write('\x00' * BlocksNum * 4)

while (InFile.tell() < FileSize):
    RawData = InFile.read(BlockSize)
    TransBits = CompressBrute(RawData)
    EncodedData = ApplySelected(RawData, TransBits)
    BlocksHeader += struct.pack('>I', (TransBits << 24) | len(EncodedData))
    OutFile.write(EncodedData)

OutFile.seek(2, 0)
OutFile.write(BlocksHeader)

InFile.close()
OutFile.close()
