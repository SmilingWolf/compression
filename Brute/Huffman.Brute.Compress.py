import sys, struct, math

# Since we are using runlength fields up to 2 bytes long
# 65535 is the max value we can store in them, so divide
# the input stream in blocks of that length.
BlockSize = 65535

def BWTEncode(Text):
    N = len(Text)
    Text2 = Text * 2
    class K:
        def __init__(self, i):
            self.i = i
        def __lt__(a, b):
            i, j = a.i, b.i
            for k in xrange(N):
                if Text2[i + k] < Text2[j + k]:
                    return True
                elif Text2[i + k] > Text2[j + k]:
                    return False
            return False
    InOrder = sorted(range(N), key = K)
    Index = InOrder.index(0)
    OutData = struct.pack('>H', Index)
    OutData += ''.join(Text2[Elem + N - 1] for Elem in InOrder)
    return OutData

def MTFEncode(Data):
    Dict = []
    NewList = []
    for Elem in xrange(256):
        Dict.append(chr(Elem))
    for Elem in Data:
        Index = Dict.index(Elem)
        NewList.append(chr(Index))
        Dict.pop(Index)
        Dict.insert(0, Elem)
    return ''.join(NewList)

def DataToBitsList(Data):
    BitsList = []
    for Elem in xrange(len(Data)):
        Char = Data[Elem]
        for x in xrange(7, -1, -1):
            BitsList.append((ord(Char) >> x) & 1)
    return BitsList

def BitsListToData(BitsList, Format, Len):
    OutData = ''
    for Elem in xrange(0, len(BitsList), Len):
        Byte = BitsList[Elem:Elem + Len]
        Byte = ''.join(str(Bit) for Bit in Byte)
        PadLen = Len - len(Byte)
        OutData += struct.pack(Format, int(Byte, 2) << PadLen)
    return OutData, PadLen

def CreateHuffmanTree(Data):
    BytesFreqDict = {}
    for Elem in Data:
        if (Elem not in BytesFreqDict):
            BytesFreqDict[Elem] = 1
        else:
            BytesFreqDict[Elem] += 1

    NodeList = sorted(BytesFreqDict.items(), key = lambda field: field[1], reverse = True)

    while (len(NodeList) > 1):
        Left = NodeList.pop()
        Right = NodeList.pop()
        Parent = []
        Parent.append(Right)
        Parent.append(Left[1] + Right[1])
        Parent.append(Left)
        NodeList.append(Parent)
        NodeList = sorted(NodeList, key = lambda field: field[1], reverse = True)
    return NodeList

def CreateHuffmanDict(List, Symbol = '', SymDict = {}):
    if (len(List[0]) == 3):
        CreateHuffmanDict(List[0], Symbol + '0')
    elif (len(List[0]) == 2):
        SymDict[List[0][0]] = Symbol + '0'
    if (len(List[2]) == 3):
        CreateHuffmanDict(List[2], Symbol + '1')
    elif (len(List[2]) == 2):
        SymDict[List[2][0]] = Symbol + '1'
    return SymDict

def EncodeTree(List):
    SymList = []
    if (len(List[0]) == 3):
        SymList.append(0)
        for Elem in (EncodeTree(List[0])):
            SymList.append(Elem)
    elif (len(List[0]) == 2):
        SymList.append(1)
        for Elem in DataToBitsList(List[0][0]):
            SymList.append(Elem)
    if (len(List[2]) == 3):
        SymList.append(0)
        for Elem in (EncodeTree(List[2])):
            SymList.append(Elem)
    elif (len(List[2]) == 2):
        SymList.append(1)
        for Elem in DataToBitsList(List[2][0]):
            SymList.append(Elem)
    return SymList

def HuffmanEncode(Data):
    HuffmanTree = CreateHuffmanTree(Data)
    CodesDict = CreateHuffmanDict(HuffmanTree[0])
    EncodedTreeList = EncodeTree(HuffmanTree[0])
    EncodedTree = BitsListToData(EncodedTreeList, '>B', 8)[0]

    Message = ''
    for Elem in Data:
        Message += CodesDict[Elem]

    OutData, PadLen = BitsListToData(Message, '>I', 32)
    OutData = struct.pack('>H', len(EncodedTreeList)) + EncodedTree + chr(PadLen) + OutData
    return OutData

def ApplySelected(Data, TransBits):
    BoolCompress = TransBits & 1
    BoolMTF = (TransBits >> 1) & 1
    BoolBWT = (TransBits >> 2) & 1
    Encoded = Data
    if (BoolBWT == 1 and BoolCompress == 1):
        Encoded = BWTEncode(Encoded)
    if (BoolMTF == 1 and BoolCompress == 1):
        Encoded = MTFEncode(Encoded)
    if (BoolCompress == 1):
        Encoded = HuffmanEncode(Encoded)
    return Encoded

def CompressBrute(Data):
    CompressResDict = {}
    for TransBits in xrange(8):
        CompressResDict[TransBits] = len(ApplySelected(Data, TransBits))
    CompressResults = sorted(CompressResDict.items(), key = lambda field: field[1])
    return CompressResults[0][0]

InFile = open(sys.argv[1], 'rb')
OutFile = open(sys.argv[2], 'wb')

InFile.seek(0, 2)
FileSize = InFile.tell()
InFile.seek(0, 0)

BlocksHeader = b''
BlocksNum = int(math.ceil(FileSize / float(BlockSize)))
OutFile.write(struct.pack('>H', BlocksNum))
OutFile.write('\x00' * BlocksNum * 4)

while (InFile.tell() < FileSize):
    RawData = InFile.read(BlockSize)
    TransBits = CompressBrute(RawData)
    EncodedData = ApplySelected(RawData, TransBits)
    BlocksHeader += struct.pack('>I', (TransBits << 24) | len(EncodedData))
    OutFile.write(EncodedData)

OutFile.seek(2, 0)
OutFile.write(BlocksHeader)

InFile.close()
OutFile.close()
