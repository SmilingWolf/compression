import sys, struct

# Since we are using runlength fields of up to 2 bytes long
# 65535 is the max value we can store in them, so divide
# the input stream in blocks of that length.
BlockSize = 65535

def MTFDecode(Data):
    Dict = []
    NewList = []
    for Elem in xrange(256):
        Dict.append(chr(Elem))
    for Elem in Data:
        Index = ord(Elem)
        NewList.append(Dict[Index])
        Dict.insert(0, Dict[Index])
        Dict.pop(Index + 1)
    return ''.join(NewList)

def RLEDecode(Data):
    ByteType = 0
    DataIndex = 0
    RunLength = 0
    OutData = b''
    while (DataIndex < len(Data)):
        ByteType = struct.unpack('>B', Data[DataIndex])[0]
        if (ByteType == 0):
            OutData += Data[DataIndex + 1]
            DataIndex += 2
        elif (ByteType == 1):
            OutData += Data[DataIndex + 1] + Data[DataIndex + 2]
            DataIndex += 3
        elif (ByteType == 2):
            RunLength = struct.unpack('>B', Data[DataIndex + 1])[0]
            OutData += Data[DataIndex + 2] * RunLength
            DataIndex += 3
        elif (ByteType == 3):
            RunLength = Data[DataIndex + 1] + Data[DataIndex + 2]
            RunLength = struct.unpack('>H', RunLength)[0]
            OutData += Data[DataIndex + 3] * RunLength
            DataIndex += 4
    return OutData

InFile = open(sys.argv[1], 'rb')
OutFile = open(sys.argv[2], 'wb')

BlocksNum = struct.unpack('>H', InFile.read(2))[0]
BlocksHeader = InFile.read(BlocksNum * 4)

BlocksList = []
FormatString = '>%dI' % BlocksNum
BlocksInfos = struct.unpack(FormatString, BlocksHeader)
for Elem in BlocksInfos:
    TransBits = Elem >> 24
    BlockSize = Elem & 0xFFFFFF
    BlocksList.append((TransBits, BlockSize))

for Elem in BlocksList:
    Data = InFile.read(Elem[1])
    DecodedData = Data
    if ((Elem[0] >> 0) & 1 == 1):
        DecodedData = RLEDecode(DecodedData)
    if ((Elem[0] >> 1) & 1 == 1):
        DecodedData = MTFDecode(DecodedData)
    OutFile.write(DecodedData)

InFile.close()
OutFile.close()
