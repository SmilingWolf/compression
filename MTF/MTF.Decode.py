#!/usr/bin/env python

import sys

def MTFDecode(Data):
    Dict = []
    NewList = []
    for Elem in xrange(256):
        Dict.append(chr(Elem))
    for Elem in Data:
        Index = ord(Elem)
        NewList.append(Dict[Index])
        Dict.insert(0, Dict[Index])
        Dict.pop(Index + 1)
    return ''.join(NewList)

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

DecodedData = MTFDecode(InData)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(DecodedData)
OutFile.close()
