#!/usr/bin/env python

import sys

def MTFEncode(Data):
    Dict = []
    NewList = []
    for Elem in xrange(256):
        Dict.append(chr(Elem))
    for Elem in Data:
        Index = Dict.index(Elem)
        NewList.append(chr(Index))
        Dict.pop(Index)
        Dict.insert(0, Elem)
    return ''.join(NewList)

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

EncodedData = MTFEncode(InData)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(EncodedData)
OutFile.close()
