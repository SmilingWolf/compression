#!/usr/bin/env python

import sys, struct

def RLEEncode(Data):
    DataIndex = 0
    RunCounter = 0
    LastByte = b''
    TupleList = []
    OutData = b''
    if (DataIndex < len(Data) and LastByte == b''):
        LastByte = Data[DataIndex]
    while (DataIndex < len(Data)):
        if (Data[DataIndex] != LastByte):
            EncodedTuple = (RunCounter, LastByte)
            TupleList.append(EncodedTuple)
            RunCounter = 1
            LastByte = Data[DataIndex]
        else:
            RunCounter += 1
        DataIndex += 1
    if (len(Data) > 0):
        EncodedTuple = (RunCounter, LastByte)
        TupleList.append(EncodedTuple)
    for Elem in TupleList:
        if (Elem[0] == 1):
            OutData += struct.pack('>B', 0)
        elif (Elem[0] > 1 and Elem[0] < 256):
            OutData += struct.pack('>B', 1)
            OutData += struct.pack('>B', Elem[0])
        else:
            OutData += struct.pack('>B', 2)
            OutData += struct.pack('>H', Elem[0])
        OutData += Elem[1]
    return OutData

InFile = open(sys.argv[1], 'rb')
RawData = InFile.read()
InFile.close()

EncodedData = RLEEncode(RawData)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(EncodedData)
OutFile.close()
