#!/usr/bin/env python

import sys, struct

def RLEDecode(Data):
    ByteType = 0
    DataIndex = 0
    RunLength = 0
    OutData = b''
    while (DataIndex < len(Data)):
        ByteType = struct.unpack('>B', Data[DataIndex])[0]
        if (ByteType == 0):
            OutData += Data[DataIndex + 1]
            DataIndex += 2
        elif (ByteType == 1):
            RunLength = struct.unpack('>B', Data[DataIndex + 1])[0]
            OutData += Data[DataIndex + 2] * RunLength
            DataIndex += 3
        elif (ByteType == 2):
            RunLength = Data[DataIndex + 1] + Data[DataIndex + 2] 
            RunLength = struct.unpack('>H', RunLength)[0]
            OutData += Data[DataIndex + 3] * RunLength
            DataIndex += 4
    return OutData

InFile = open(sys.argv[1], 'rb')
Data = InFile.read()
InFile.close()

DecodedData = RLEDecode(Data)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(DecodedData)
OutFile.close()
