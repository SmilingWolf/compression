#!/usr/bin/env python

import sys, struct

# Since we are using runlength fields of up to 2 bytes long
# 65535 is the max value we can store in them, so divide
# the input stream in blocks of that length.
BlockSize = 65535

def GetRunLength(Data, Index):
    RunLength = 1
    while (Data[Index] == Data[Index + 1]):
        RunLength += 1
        Index += 1
        if (Index + 1 == len(Data)):
            break
    return RunLength

def RLEEncode(Data):
    DataIndex = 0
    RunCounter = 0
    TupleList = []
    OutData = b''
    while (DataIndex + 1 < len(Data)):
        RunCounter = GetRunLength(Data, DataIndex)
        if (RunCounter <= 2):
            Digram = Data[DataIndex] + Data[DataIndex + 1]
            EncodedTuple = (2, Digram)
            if (RunCounter == 1):
                DataIndex += 1
        else:
            EncodedTuple = (RunCounter, Data[DataIndex])
        TupleList.append(EncodedTuple)
        DataIndex += RunCounter
    if (DataIndex < len(Data)):
        EncodedTuple = (1, Data[DataIndex])
        TupleList.append(EncodedTuple)
        DataIndex += 1
    for Elem in TupleList:
        if (Elem[0] == 1):
            OutData += struct.pack('>B', 0)
        elif (Elem[0] == 2):
            OutData += struct.pack('>B', 1)
        elif (Elem[0] > 2 and Elem[0] < 256):
            OutData += struct.pack('>B', 2)
            OutData += struct.pack('>B', Elem[0])
        else:
            OutData += struct.pack('>B', 3)
            OutData += struct.pack('>H', Elem[0])
        OutData += Elem[1]
    return OutData

InFile = open(sys.argv[1], 'rb')
OutFile = open(sys.argv[2], 'wb')

InFile.seek(0, 2)
FileSize = InFile.tell()
InFile.seek(0, 0)

while (InFile.tell() + BlockSize <= FileSize):
    RawData = InFile.read(BlockSize)
    EncodedData = RLEEncode(RawData)
    OutFile.write(EncodedData)

if (InFile.tell() < FileSize):
    RawData = InFile.read(BlockSize)
    EncodedData = RLEEncode(RawData)
    OutFile.write(EncodedData)

InFile.close()
OutFile.close()
