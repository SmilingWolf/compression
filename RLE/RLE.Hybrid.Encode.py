#!/usr/bin/env python

import sys, struct

def GetRunLength(Data, Index):
    RunLength = 1
    while (Data[Index] == Data[Index + 1]):
        RunLength += 1
        Index += 1
        if (Index + 1 == len(Data)):
            break
    return RunLength

def RLEEncode(Data):
    DataIndex = 0
    RunCounter = 0
    TupleList = []
    OutData = b''
    while (DataIndex + 1 < len(Data)):
        RunCounter = GetRunLength(Data, DataIndex)
        if (RunCounter <= 2):
            Digram = Data[DataIndex] + Data[DataIndex + 1]
            EncodedTuple = (RunCounter, Digram)
            if (RunCounter == 1):
                DataIndex += 1
        else:
            EncodedTuple = (RunCounter, Data[DataIndex])
        TupleList.append(EncodedTuple)
        DataIndex += RunCounter
    if (DataIndex < len(Data)):
        EncodedTuple = (1, Data[DataIndex])
        TupleList.append(EncodedTuple)
        DataIndex += 1
    for Elem in TupleList:
        if (Elem[0] <= 2):
            OutData += struct.pack('>B', 0)
        elif (Elem[0] > 2 and Elem[0] < 256):
            OutData += struct.pack('>B', 1)
            OutData += struct.pack('>B', Elem[0])
        else:
            OutData += struct.pack('>B', 2)
            OutData += struct.pack('>H', Elem[0])
        OutData += Elem[1]
    return OutData

InFile = open(sys.argv[1], 'rb')
RawData = InFile.read()
InFile.close()

EncodedData = RLEEncode(RawData)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(EncodedData)
OutFile.close()
