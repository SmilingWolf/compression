# Thanks to the many people on SO, this code is theirs.
# I'll link to the appropriate SO answers where these come from
# as soon as I find them again.
# EDIT: http://stackoverflow.com/a/21298629

import sys, struct, functools

def BWTKey(Text, Value, Step):
    return Text[(Value + Step) % len(Text)]

def RadixSort(Values, Key, Step = 0):
    if len(Values) < 2:
        for Value in Values:
            yield Value
        return
    Bins = {}
    for Value in Values:
        Bins.setdefault(Key(Value, Step), []).append(Value)
    for k in sorted(Bins.keys()):
        for r in RadixSort(Bins[k], Key, Step + 1):
            yield r

def BWTEncode(Text):
    LastColumnsChars = []
    IndexesList = []
    for Elem in RadixSort(range(len(Text)), functools.partial(BWTKey, Text)):
        IndexesList.append(Elem)
        LastColumnsChars.append(Text[Elem - 1])
    Index = IndexesList.index(0)
    return Index, ''.join(LastColumnsChars)

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

Index, EncodedData = BWTEncode(InData)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(struct.pack('>H', Index))
OutFile.write(EncodedData)
OutFile.close()
