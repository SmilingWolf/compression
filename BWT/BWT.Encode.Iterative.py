# Thanks to the many people on SO, this code is theirs.
# I'll link to the appropriate SO answers where these come from
# as soon as I find them again.
# EDIT: http://stackoverflow.com/a/21298992

import sys, struct

def BWTEncode(Text):
    N = len(Text)
    Text2 = Text * 2
    class K:
        def __init__(self, i):
            self.i = i
        def __lt__(a, b):
            i, j = a.i, b.i
            for k in xrange(N):
                if Text2[i + k] < Text2[j + k]:
                    return True
                elif Text2[i + k] > Text2[j + k]:
                    return False
            return False
    InOrder = sorted(range(N), key = K)
    Index = InOrder.index(0)
    return Index, ''.join(Text2[Elem + N - 1] for Elem in InOrder)

InFile = open(sys.argv[1], 'rb')
InData = InFile.read()
InFile.close()

Index, EncodedData = BWTEncode(InData)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(struct.pack('>H', Index))
OutFile.write(EncodedData)
OutFile.close()
