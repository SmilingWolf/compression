# Thanks to the many people on SO, this code is theirs.
# I'll link to the appropriate SO answers where these come from
# as soon as I find them again.
# EDIT: http://codereview.stackexchange.com/a/21120

import sys, struct

def BWTDecode(k, s):
    def Row(k):
        Permutation = sorted((t, i) for i, t in enumerate(s))
        for _ in s:
            t, k = Permutation[k]
            yield t
    return ''.join(Row(k))

InFile = open(sys.argv[1], 'rb')
InData = InFile.read(2)
Index = struct.unpack('>H', InData)[0]
InData = InFile.read()
InFile.close()

Transformed = BWTDecode(Index, InData)

OutFile = open(sys.argv[2], 'wb')
OutFile.write(Transformed)
OutFile.close()
